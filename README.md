# cisco-configurations



## Getting started

This is the place to store the vars we will use for each device and the jsonschema files to validate the configurations.

~~~
hostname 
   schemas
     hostname.json
   vars
     host_vars.yml
~~~~
